<?php

namespace Weeny\Lib\Collection\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Contract\Collection\CollectionInterface;

abstract class CollectionTest extends TestCase
{

    abstract public function dataProviderNotEmptyCollection();

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testArrayAccess(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        foreach ($expectedElements as $index => $expectedValue) {
            $this->assertEquals($expectedValue, $collection[$index]);
        }
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testForeach(CollectionInterface$collection, $expectedElements, $unexistsValue = null) {
        foreach ($collection as $index => $element) {
            $this->assertEquals($expectedElements[$index], $element);
        }
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testIsset(CollectionInterface$collection, $expectedElements, $unexistsValue = null) {
        foreach ($expectedElements as $index => $expectedValue) {
            $this->assertTrue(isset($collection[$index]));
        }
        $index++;
        $this->assertFalse(isset($collection[$index]));
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testUnset(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $index = rand(0, count($expectedElements));
        unset($expectedElements[$index]);
        unset($collection[$index]);
        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testSet(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $existsIndex = rand(0, count($expectedElements)-1);
        $index = rand(count($expectedElements)+1, count($expectedElements)+10);

        $expectedElements[$index] = $expectedElements[$existsIndex];
        $collection[$index] = $collection[$existsIndex];
        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testRewind(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        if (count($expectedElements) <= 1) {
            $this->markTestSkipped('Need more elements into collection for this test');
        }

        $this->assertEquals($expectedElements[0], $collection->current());
        $collection->next();
        $this->assertEquals($expectedElements[1], $collection->current());
        $collection->rewind();
        $this->assertEquals($expectedElements[0], $collection->current());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testContains(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $expectedElements = array_unique($expectedElements, SORT_REGULAR);
        $collection->unique();
        $index = rand(0, count($expectedElements)-1);

        $this->assertTrue($collection->contains($expectedElements[$index]));
        unset($collection[$index]);
        $this->assertFalse($collection->contains($expectedElements[$index]));
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testPush(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $index = rand(0, count($expectedElements)-1);

        array_push($expectedElements, $expectedElements[$index]);
        $collection->push($expectedElements[$index]);

        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testPushArrayAccess(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $index = rand(0, count($expectedElements)-1);

        $expectedElements[] =  $expectedElements[$index];
        $collection[] = $expectedElements[$index];

        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testClear(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $this->assertFalse($collection->isEmpty());
        $collection->clear();
        $this->assertTrue($collection->isEmpty());
        $this->assertCount(0, $collection);
        $this->assertFalse(isset($collection[0]));
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testRemoveByIndex(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $index = rand(0, count($expectedElements)-1);

        $expectedValue = $expectedElements[$index];
        unset($expectedElements[$index]);
        $this->assertEquals($expectedValue, $collection->removeByIndex($index));

        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testRemoveByUnexistIndex(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $index = rand(count($expectedElements), count($expectedElements) + 10);

        $this->assertNull($collection->removeByIndex($index));

        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testRemoveByUnexistsValue(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        if ( is_null($unexistsValue) ) {
            $this->markTestSkipped('Need Unexists values for this tests');
        }

        $this->assertFalse($collection->removeByValue($unexistsValue));
        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testRemoveByValue(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $expectedElements = array_unique($expectedElements, SORT_REGULAR);
        $collection->unique();

        $index = rand(0, count($expectedElements)-1);
        $value = $expectedElements[$index];
        unset($expectedElements[$index]);
        $this->assertTrue($collection->removeByValue($value));
        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testPop(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $value = array_pop($expectedElements);
        $this->assertEquals($value, $collection->pop());
        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testShift(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $value = array_shift($expectedElements);
        $this->assertEquals($value, $collection->shift());
        $this->assertEquals($expectedElements, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testUnshift(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        $index = rand(0, count($expectedElements)-1);

        $value = $expectedElements[$index];
        array_unshift($expectedElements, $value);
        $collection->unShift($value);
        $this->assertEquals($expectedElements, $collection->toArray());
    }
    /**
     * @dataProvider dataProviderNotEmptyCollection
     */
    public function testReverce(CollectionInterface $collection, $expectedElements, $unexistsValue = null) {
        array_reverse($expectedElements);
        $collection->reverce();
        $this->assertEquals($expectedElements, $collection->toArray());
    }

}
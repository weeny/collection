<?php

namespace Weeny\Lib\Collection\Tests;

use Weeny\Lib\Collection\MixedCollection;

class MixedCollectionTest extends CollectionTest
{

    public function dataProviderNotEmptyCollection() {
        return [
            [
                new MixedCollection(1,2,'three', 'four',1),
                [1,2,'three', 'four',1],
                'tree'
            ]
        ];
    }

}
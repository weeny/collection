<?php
namespace Weeny\Lib\Collection\Tests;

use Weeny\Contract\Collection\CollectionInterface;

abstract class TypedCollectionTest extends CollectionTest
{

    abstract public function dataProviderForCreatePositive();
    abstract public function dataProviderForCreateNegative();
    abstract public function dataProviderForManipulateNegative();

    /**
     * @dataProvider dataProviderForCreatePositive
     */
    public function testCreatePositive(string $className, array $expectedValues) {
        $collection = new $className(...$expectedValues);
        $this->assertEquals($expectedValues, $collection->toArray());
    }

    /**
     * @dataProvider dataProviderForCreateNegative
     */
    public function testCreateNegative(string $className, array $expectedValues) {
        $this->expectException(\TypeError::class);
        new $className(...$expectedValues);
    }

    /**
     * @dataProvider dataProviderForManipulateNegative
     */
    public function testIndexOfNegative(CollectionInterface $collection, $serchedValue) {
        $this->expectException(\TypeError::class);
        $collection->indexOf($serchedValue);
    }

    /**
     * @dataProvider dataProviderForManipulateNegative
     */
    public function testUnShiftNegative(CollectionInterface $collection, $serchedValue) {
        $this->expectException(\TypeError::class);
        $collection->unShift($serchedValue);
    }

    /**
     * @dataProvider dataProviderForManipulateNegative
     */
    public function testRemoveByValueNegative(CollectionInterface $collection, $serchedValue) {
        $this->expectException(\TypeError::class);
        $collection->removeByValue($serchedValue);
    }

    /**
     * @dataProvider dataProviderForManipulateNegative
     */
    public function testPushNegative(CollectionInterface $collection, $serchedValue) {
        $this->expectException(\TypeError::class);
        $collection->push($serchedValue);
    }

    /**
     * @dataProvider dataProviderForManipulateNegative
     */
    public function testContainsNegative(CollectionInterface $collection, $serchedValue) {
        $this->expectException(\TypeError::class);
        $collection->contains($serchedValue);
    }

    /**
     * @dataProvider dataProviderForManipulateNegative
     */
    public function testSetNegative(CollectionInterface $collection, $serchedValue) {
        $this->expectException(\TypeError::class);
        $collection[0] = $serchedValue;
    }

}
<?php

namespace Weeny\Lib\Collection\Tests;

use Weeny\Lib\Collection\StringCollection;

class StringCollectionTest extends TypedCollectionTest
{

    public function dataProviderNotEmptyCollection()
    {
        return [
            [
                new StringCollection('one', 'two', 'three', 'four', 'one'),
                ['one', 'two', 'three', 'four', 'one'],
                'five'
            ]
        ];
    }

    public function dataProviderForCreatePositive()
    {
        return [
            [
                StringCollection::class,
                ['one', 'two', 'three', 'four', 'one']
            ], [
                StringCollection::class,
                []
            ]
        ];
    }

    public function dataProviderForCreateNegative()
    {
        return [
            [
                StringCollection::class,
                ['one', new \DateTime('now'), 'three', 'four', 'one']
            ], [
                StringCollection::class,
                [new \stdClass()]
            ]
        ];
    }

    public function dataProviderForManipulateNegative()
    {
        return [
            [
                new StringCollection('one', 'two', 'three', 'four', 'one'),
                new \stdClass()
            ]
        ];
    }
}
<?php
declare(strict_types=1);

namespace Weeny\Lib\Collection;

use Weeny\Contract\Collection\StringCollectionInterface;

class StringCollection extends AbstractCollection implements StringCollectionInterface
{
    public function __construct(string ...$elements)
    {
        parent::__construct(...$elements);
    }

    public function current(): string
    {
        return parent::current();
    }

    public function offsetGet($offset): string
    {
        return parent::offsetGet($offset);
    }

    public function removeByIndex(int $index): ?string {
        return parent::removeByIndex($index);
    }

    /**
     * @inheritDoc
     */
    public function pop(): string {
        return parent::pop();
    }

    /**
     * @inheritDoc
     */
    public function shift(): string {
        return parent::shift();
    }

    /**
     * @inheritDoc
     */
    protected function checkType($element, string $message): void
    {
        if ( !is_string($element) )
        {
            throw new \TypeError(sprintf($message, 'string'));
        }
    }
}
<?php
namespace Weeny\Lib\Collection;

use Weeny\Contract\Collection\CollectionInterface;

abstract class AbstractCollection implements CollectionInterface
{

    /**
     * @var array
     */
    protected $elements;

    /**
     * @var \Generator
     */
    protected $generator;

    /**
     * Check that element type is correct for this collection
     * @param $element
     * @return mixed
     * @throws \TypeError
     */
    abstract protected function checkType($element, string $message): void;

    public function __construct(...$elements)
    {
        $this->elements = $elements;
        $this->generator = $this->createGeneratorFromArray();
    }


    protected function createGeneratorFromArray(): \Generator
    {
        foreach ($this->elements as $element) {
            yield $element;
        }
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->generator->current();
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        $this->generator->next();
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->generator->key();
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return $this->generator->valid();
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->generator = $this->createGeneratorFromArray();
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return isset($this->elements[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        return $this->elements[$offset];
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value): void
    {
        $this->checkType($value, 'Argument 2 must bee %s');
        if ( is_numeric($offset) ) {
            $this->elements[$offset] = $value;
        } else {
            $this->elements[] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset)
    {
        unset($this->elements[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function contains($value): bool {
        $this->checkType($value, 'Argument 1 must bee %s');
        return in_array($value, $this->elements);
    }

    /**
     * @inheritDoc
     */
    public function push($value): void
    {
        $this->checkType($value, 'Argument 1 must bee %s');
        $this->elements[] = $value;
    }

    /**
     * @inheritDoc
     */
    public function clear(): void
    {
        $this->elements = [];
        $this->rewind();
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return count($this->elements) == 0;
    }

    /**
     * @inheritDoc
     */
    public function removeByIndex(int $index)
    {
        if ( !isset($this->elements[$index]) ) {
            return null;
        }

        $value = $this->elements[$index];
        unset($this->elements[$index]);
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function removeByValue($value): bool
    {
        $this->checkType($value, 'Argument 1 must bee typeof %s');
        $index = $this->indexOf($value);
        if ( is_null($index) ) {
            return false;
        }

        $this->removeByIndex($index);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function pop()
    {
        return array_pop($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function shift()
    {
        return array_shift($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function unShift(...$values)
    {
        $index = 0;
        foreach ($values as $value) {
            $this->checkType($value, 'Argument '.$index.' must bee typeof %s');
            $index++;
        }
        array_unshift($this->elements, ...$values);
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return $this->elements;
    }

    /**
     * @inheritDoc
     */
    public function reverce(): void
    {
        array_reverse($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function unique(): void
    {
        $this->elements = array_unique($this->elements, SORT_REGULAR);
        $this->rewind();
    }

    /**
     * @inheritDoc
     */
    public function indexOf($value): ?int
    {
        $this->checkType($value, 'Argument must be type of %s');

        $index = 0;
        foreach ($this->elements as $element) {
            if ($element == $value) {
                return $index;
            }
            $index++;
        }

        return null;
    }
}
<?php

namespace Weeny\Lib\Collection;

use Weeny\Contract\Collection\CollectionInterface;

class MixedCollection extends AbstractCollection implements CollectionInterface
{

    /**
     * @inheritDoc
     */
    protected function checkType($element, string $message): void
    {

    }
}